defmodule Votebot.Repo.Migrations.CreateVotes do
  use Ecto.Migration

  def change do
    create table(:votes) do
      # TODO: on delete?
      add :poll_option_id, references(:poll_options, on_delete: :nothing)
      add :user_id, :string, null: false
      # TODO: on delete?
      add :poll_id, references(:polls, on_delete: :nothing)

      timestamps()
    end

    create index(:votes, [:user_id])
    create index(:votes, [:poll_id])
    create index(:votes, [:poll_option_id])
  end
end
