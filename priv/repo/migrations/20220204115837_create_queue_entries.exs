defmodule Votebot.Repo.Migrations.CreateQueueEntries do
  use Ecto.Migration

  def change do
    create table(:queue_entries) do
      add :order, :integer
      # TODO: on delete?
      add :speaking_queue_id, references(:speaking_queues, on_delete: :nothing)
      add :user_id, :string
      # add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end

    create index(:queue_entries, [:speaking_queue_id])
    create index(:queue_entries, [:user_id])
  end
end
