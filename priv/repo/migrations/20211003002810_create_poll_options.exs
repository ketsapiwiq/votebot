defmodule Votebot.Repo.Migrations.CreatePollOptions do
  use Ecto.Migration

  def change do
    create table(:poll_options) do
      add :name, :string
      # TODO: on delete of poll, delete poll options
      add :poll_id, references(:polls, on_delete: :nothing)
      add :description, :text
      add :number, :integer

      timestamps()
    end

    create index(:poll_options, [:poll_id])
    create unique_index(:poll_options, [:name, :poll_id])
    create unique_index(:poll_options, [:number, :poll_id])
  end
end
