defmodule Votebot.Repo.Migrations.CreatePolls do
  use Ecto.Migration

  def change do
    create table(:polls) do
      add :slug, :string
      add :description, :text
      add :room_id, :string, null: false
      add :creator, :string
      # add :room_id, references("rooms")
      timestamps()
    end

    create index(:polls, [:room_id])
    create index(:polls, [:creator])
    # create unique_index(:polls, [:slug])
  end
end
