defmodule Votebot.Repo.Migrations.CreateSpeakingQueues do
  use Ecto.Migration

  def change do
    create table(:speaking_queues) do
      add :type, :string
      add :poll_id, references(:polls, on_delete: :nothing)

      timestamps()
    end

    create index(:speaking_queues, [:poll_id])
  end
end
