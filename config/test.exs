import Config

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
config :votebot, Votebot.Repo,
  username: "postgres",
  password: "postgres",
  database: "votebot_test#{System.get_env("MIX_TEST_PARTITION")}",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox,
  pool_size: 10

# Print only warnings and errors during test
config :logger, level: :warn


config :votebot, :matrix,
  base_url: "https://matrix.fuz.re",
  access_token: "syt_dm90ZWJvdA_IBNGEpsdiwwtLcZLVnig_14Ng3h",
  user_id: "@votebot:matrix.fuz.re"
