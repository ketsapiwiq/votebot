import Config

# Configure your database
config :votebot, Votebot.Repo,
  hostname: System.get_env("POSTGRES_HOST") || "localhost",
  username: "postgres",
  password: "postgres",
  database: "votebot_dev",
  hostname: "localhost",
  show_sensitive_data_on_connection_error: true,
  pool_size: 10

# Do not include metadata nor timestamps in development logs
config :logger, :console, format: "[$level] $message\n"

config :votebot, :matrix,
  base_url: "https://matrix.fuz.re",
  access_token: "syt_dm90ZWJvdA_IBNGEpsdiwwtLcZLVnig_14Ng3h",
  user_id: "@votebot:matrix.fuz.re"
