defmodule Votebot.PollsTest do
  use Votebot.DataCase

  alias Votebot.Polls

  describe "polls" do
    alias Votebot.Polls.Poll

    import Votebot.PollsFixtures

    @invalid_attrs %{description: nil, name: nil, poll_options: nil}

    test "list_polls/0 returns all polls" do
      poll = poll_fixture()
      assert Polls.list_polls() == [poll]
    end

    test "get_poll!/1 returns the poll with given id" do
      poll = poll_fixture()
      assert Polls.get_poll!(poll.id) == poll
    end

    test "create_poll/1 with valid data creates a poll" do
      valid_attrs = %{
        description: "some description",
        name: "some name",
        poll_options: "some poll_options"
      }

      assert {:ok, %Poll{} = poll} = Polls.create_poll(valid_attrs)
      assert poll.description == "some description"
      assert poll.name == "some name"
      assert poll.poll_options == "some poll_options"
    end

    test "create_poll/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Polls.create_poll(@invalid_attrs)
    end

    test "update_poll/2 with valid data updates the poll" do
      poll = poll_fixture()

      update_attrs = %{
        description: "some updated description",
        name: "some updated name",
        poll_options: "some updated poll_options"
      }

      assert {:ok, %Poll{} = poll} = Polls.update_poll(poll, update_attrs)
      assert poll.description == "some updated description"
      assert poll.name == "some updated name"
      assert poll.poll_options == "some updated poll_options"
    end

    test "update_poll/2 with invalid data returns error changeset" do
      poll = poll_fixture()
      assert {:error, %Ecto.Changeset{}} = Polls.update_poll(poll, @invalid_attrs)
      assert poll == Polls.get_poll!(poll.id)
    end

    test "delete_poll/1 deletes the poll" do
      poll = poll_fixture()
      assert {:ok, %Poll{}} = Polls.delete_poll(poll)
      assert_raise Ecto.NoResultsError, fn -> Polls.get_poll!(poll.id) end
    end

    test "change_poll/1 returns a poll changeset" do
      poll = poll_fixture()
      assert %Ecto.Changeset{} = Polls.change_poll(poll)
    end
  end

  describe "poll_options" do
    alias Votebot.Polls.PollOption

    import Votebot.PollsFixtures

    @invalid_attrs %{value: nil}

    test "list_poll_options/0 returns all poll_options" do
      poll_option = poll_option_fixture()
      assert Polls.list_poll_options() == [poll_option]
    end

    test "get_poll_option!/1 returns the poll_option with given id" do
      poll_option = poll_option_fixture()
      assert Polls.get_poll_option!(poll_option.id) == poll_option
    end

    test "create_poll_option/1 with valid data creates a poll_option" do
      valid_attrs = %{value: "some value"}

      assert {:ok, %PollOption{} = poll_option} = Polls.create_poll_option(valid_attrs)
      assert poll_option.value == "some value"
    end

    test "create_poll_option/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Polls.create_poll_option(@invalid_attrs)
    end

    test "update_poll_option/2 with valid data updates the poll_option" do
      poll_option = poll_option_fixture()
      update_attrs = %{value: "some updated value"}

      assert {:ok, %PollOption{} = poll_option} =
               Polls.update_poll_option(poll_option, update_attrs)

      assert poll_option.value == "some updated value"
    end

    test "update_poll_option/2 with invalid data returns error changeset" do
      poll_option = poll_option_fixture()
      assert {:error, %Ecto.Changeset{}} = Polls.update_poll_option(poll_option, @invalid_attrs)
      assert poll_option == Polls.get_poll_option!(poll_option.id)
    end

    test "delete_poll_option/1 deletes the poll_option" do
      poll_option = poll_option_fixture()
      assert {:ok, %PollOption{}} = Polls.delete_poll_option(poll_option)
      assert_raise Ecto.NoResultsError, fn -> Polls.get_poll_option!(poll_option.id) end
    end

    test "change_poll_option/1 returns a poll_option changeset" do
      poll_option = poll_option_fixture()
      assert %Ecto.Changeset{} = Polls.change_poll_option(poll_option)
    end
  end
end
