defmodule Votebot.ChatbotTest do
  use Votebot.DataCase
  alias Votebot.Chatbot

  describe "chatbot" do

    # import Votebot.ChatbotFixtures

    @invalid_attrs %{description: nil, name: nil, poll_options: nil}

    test "handle_message with !poll new creates a poll" do

      assert {:ok, _} = Chatbot.handle_message("!AgKiujqIOxBWsRXxzD:matrix.fuz.re", "@lucas:matrix.fuz.re", "!poll new")
    end
  end
end
