defmodule Votebot.EventsTest do
  use Votebot.DataCase
  alias Votebot.Events
  alias Votebot.Polls

  # poll = {:polyjuice_client, :message,
  # {"!AgKiujqIOxBWsRXxzD:matrix.fuz.re", %{"content" => %{"body" => "!poll new", "msgtype" => "m.text"}, "event_id" => "$AF9ZaOppwd4Qpf5EU55HVXQduugNvqKgBhVE0U-zDkg", "origin_server_ts" => 1645370763531, "sender" => "@lucas:matrix.fuz.re", "type" => "m.room.message", "unsigned" => %{"age" => 191}}}}
  # poll = chatbot_fixture()

  describe "events" do
    import Votebot.EventsFixtures

    # @invalid_attrs %{description: nil, name: nil, poll_options: nil}

    test "handle_message with !poll new creates a poll" do
      # FIXME: actually check poll is created
      assert {:ok, %Polls.Poll{} = poll} =
               Events.handle_event(:message,
                  {"!AgKiujqIOxBWsRXxzD:matrix.fuz.re",
                   %{
                     "content" => %{"body" => "!poll new", "msgtype" => "m.text"},
                     "event_id" => "$AF9ZaOppwd4Qpf5EU55HVXQduugNvqKgBhVE0U-zDkg",
                     "origin_server_ts" => 1_645_370_763_531,
                     "sender" => "@lucas:matrix.fuz.re",
                     "type" => "m.room.message",
                     "unsigned" => %{"age" => 191}
                   }}
               )
    end
    test ~S"handle_message with '!poll 1 desc\nTralala' creates a poll" do
      assert {:ok, %Polls.Poll{}} =
        {event_type, event} = event_fixture("!poll 1 desc\nTralala")
               Events.handle_event(event_type, event)
              #  Events.handle_event(
              #    {:polyjuice_client, :message,
              #     {"!AgKiujqIOxBWsRXxzD:matrix.fuz.re",
              #      %{
              #        "content" => %{"body" => "!poll 1 desc\nTralala", "msgtype" => "m.text"},
              #        "event_id" => "$AF9ZaOppwd4Qpf5EU55HVXQduugNvqKgBhVE0U-zDkg",
              #        "origin_server_ts" => 1_645_370_763_531,
              #        "sender" => "@lucas:matrix.fuz.re",
              #        "type" => "m.room.message",
              #        "unsigned" => %{"age" => 191}
              #      }}}
              #  )
    end
    test "handle_message with '!poll_option new 1 Peut-être' creates a poll option" do
      assert {:ok, %Polls.PollOption{}} =
               Events.handle_event(:message,
                  {"!AgKiujqIOxBWsRXxzD:matrix.fuz.re",
                   %{
                     "content" => %{"body" => "!poll_option new 1 Peut-être", "msgtype" => "m.text"},
                     "event_id" => "$AF9ZaOppwd4Qpf5EU55HVXQduugNvqKgBhVE0U-zDkg",
                     "origin_server_ts" => 1_645_370_763_531,
                     "sender" => "@lucas:matrix.fuz.re",
                     "type" => "m.room.message",
                     "unsigned" => %{"age" => 191}
                   }}
               )
    end
    test "handle_message with '!vote 1 Peut-être' creates a vote" do
      assert {:ok, %Votebot.Votes.Vote{}} =
               Events.handle_event(:message,
                  {"!AgKiujqIOxBWsRXxzD:matrix.fuz.re",
                   %{
                     "content" => %{"body" => "!vote 1 Peut-être", "msgtype" => "m.text"},
                     "event_id" => "$AF9ZaOppwd4Qpf5EU55HVXQduugNvqKgBhVE0U-zDkg",
                     "origin_server_ts" => 1_645_370_763_531,
                     "sender" => "@lucas:matrix.fuz.re",
                     "type" => "m.room.message",
                     "unsigned" => %{"age" => 191}
                   }}
               )
    end
  end
end
