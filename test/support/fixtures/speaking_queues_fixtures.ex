defmodule Votebot.SpeakingQueuesFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Votebot.SpeakingQueues` context.
  """

  @doc """
  Generate a speaking_queue.
  """
  def speaking_queue_fixture(attrs \\ %{}) do
    {:ok, speaking_queue} =
      attrs
      |> Enum.into(%{
        type: "some type"
      })
      |> Votebot.SpeakingQueues.create_speaking_queue()

    speaking_queue
  end

  @doc """
  Generate a queue_entry.
  """
  def queue_entry_fixture(attrs \\ %{}) do
    {:ok, queue_entry} =
      attrs
      |> Enum.into(%{
        order: 42
      })
      |> Votebot.SpeakingQueues.create_queue_entry()

    queue_entry
  end
end
