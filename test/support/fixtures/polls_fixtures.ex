defmodule Votebot.PollsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Votebot.Polls` context.
  """

  @doc """
  Generate a poll.
  """
  def poll_fixture(attrs \\ %{}) do
    {:ok, poll} =
      attrs
      |> Enum.into(%{
        description: "some description",
        name: "some name",
        poll_options: "some poll_options"
      })
      |> Votebot.Polls.create_poll()

    poll
  end

  @doc """
  Generate a poll_option.
  """
  def poll_option_fixture(attrs \\ %{}) do
    {:ok, poll_option} =
      attrs
      |> Enum.into(%{
        value: "some value"
      })
      |> Votebot.Polls.create_poll_option()

    poll_option
  end
end
