defmodule Votebot.VotesFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Votebot.Votes` context.
  """

  @doc """
  Generate a vote.
  """
  def vote_fixture(attrs \\ %{}) do
    {:ok, vote} =
      attrs
      |> Enum.into(%{
        value: "some value"
      })
      |> Votebot.Votes.create_vote()

    vote
  end
end
