defmodule Votebot.EventsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Votebot.Polls` context.
  """

  @doc """
  Generate a poll.
  """
  def event_fixture(body) do
    {:polyjuice_client, :message,
    {"!AgKiujqIOxBWsRXxzD:matrix.fuz.re",
     %{
       "content" => %{"body" => body, "msgtype" => "m.text"},
       "event_id" => "$AF9ZaOppwd4Qpf5EU55HVXQduugNvqKgBhVE0U-zDkg",
       "origin_server_ts" => 1_645_370_763_531,
       "sender" => "@lucas:matrix.fuz.re",
       "type" => "m.room.message",
       "unsigned" => %{"age" => 191}
     }}}
     end
    # {:ok, poll} =
    #   attrs
    #   |> Enum.into(%{
    #     description: "some description",
    #     name: "some name",
    #     poll_options: "some poll_options"
    #   })
    #   |> Votebot.Polls.create_poll()

    # poll
  end
