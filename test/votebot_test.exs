defmodule Votebot.VotebotTest do
  use Votebot.DataCase

  # Voting

  # polls_test
  test "can create a poll with the ordre du jour and a set time trigger" do
  end

  test "can create a poll with the ordre du jour and a set threshold trigger" do
  end

  test "can create a poll with the ordre du jour as ranked choice voting" do
  end

  test "can create a poll with the ordre du jour as Majority judgment voting" do
  end

  test "can add a poll option if poll admin" do
  end

  # votes_test
  test "cannot vote after poll is closed" do
  end

  test "can print current results while vote is in progress" do
  end

  test "can print final results" do
  end

  test "should not vote if user has already voted if vote can't be changed" do
    # user = User.new(id: 1, name: "Test")
    # user.votes.create(poll_id: 1, option_id: 1)
    # poll = Poll.new(id: 1, title: "Test", options: [Option.new(id: 1, text: "Test")])
    # assert_equal(false, vote(user, poll, 1))
  end

  test "should not vote if poll is not open" do
    # user = User.new(id: 1, name: "Test")
    # poll = Poll.new(id: 1, title: "Test", options: [Option.new(id: 1, text: "Test")], open: false)
    # assert_equal(false, vote(user, poll, 1))
  end

  # votebot test
  test "'!votebot create poll=' should create a poll" do
  end

  # Speaking queues

  test "can register for speaking queue for 2min with default settings" do
    #   user = User.new(id: 1, name: "Test")
    #   assert_equal true, register_for_speaking_queue(user)
  end

  test "can register for quick speaking queue for 30s with default settings" do
    #   user = User.new(id: 1, name: "Test")
    #   assert_equal true, register_for_quick_speaking_queue(user)
  end

  test "can list registered for both speaking queue and quick queue" do
    #   assert_equal true, list_registered_for_speaking_and_quick_queue(user)
  end

  test "prints a message when speaking queue is close to an end" do
    #   assert_equal true, print_message_when_speaking_queue_is_close_to_an_end(user)
  end

  test "prints an alert and the name of the next user in the list, and whether it's a quick answer or the normal speaking queue when speaking queue ends" do
  end
end

#   - Un ordre du jour et des temps prévus
# - On s'inscrit à chaque thème
# - Une période de 2min par défaut
# - Une réaction de 30s avec un système de queue
# - des types de vote : préférentiel (trié, Ranked Choice Voting), note sur 10 de chaque choix, vote à 1 tour ou 2 tours avec threshold
# - pouvoir modérer le chat en live et poser des questions par écrit
