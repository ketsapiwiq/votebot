defmodule Votebot.Events do
  @moduledoc """
  The Event context.
  """
  import Logger
  import Votebot.Votes
  import Votebot.Chatbot

  def handle_event(
        :message,
        {room_id,
         %{"content" => %{"msgtype" => msgtype, "body" => body} = content, "sender" => sender}}
      )
      when msgtype in ["m.text", "m.notice"] do
    votebot_user_id = Application.get_env(:votebot, :matrix) |> Keyword.fetch!(:user_id)
    # TODO: add switch if chatbot is enabled or not
    if sender == votebot_user_id do
      # Logger.debug("ignored event from bot")
      {:ok, :ignored_from_bot}
    else
      case handle_message(room_id, sender, String.trim(body)) do
        {:ok, state} -> state
        {:error, error} -> Logger.error("Error #{error} handling message: #{body}")
        message -> Logger.error("Error: answer '#{message}' while handling message: #{body}")
      end
    end
  end

  def handle_event(:logged_out, _details) do
    Logger.warn(
      "Logged out, did you correctly enter your user id, user token and homeserver in setting?"
    )
  end

  def handle_event(event_type, event) do
    Logger.debug("Unhandled event type: " <> inspect(event_type))
    Logger.debug("Event: " <> inspect(event))
    {:ok, :unhandled}
  end

  def handle_event(event_type) do
    Logger.debug("Unhandled event type: " <> inspect(event_type))
    {:ok, :unhandled}
  end
end
