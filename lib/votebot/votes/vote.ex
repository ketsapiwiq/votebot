defmodule Votebot.Votes.Vote do
  use Ecto.Schema
  import Ecto.Changeset
  alias Votebot.Polls.Poll
  alias Votebot.Polls.PollOption

  schema "votes" do
    belongs_to(:poll_option, PollOption)
    field :user_id, :string
    belongs_to(:poll, Poll)

    timestamps()
  end

  @doc false
  def changeset(vote, attrs) do
    vote
    |> cast(attrs, [:poll_option_id, :poll_id, :user_id])
    |> validate_required([:poll_option_id, :poll_id, :user_id])
  end
end
