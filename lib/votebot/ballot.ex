defmodule Votebot.Polls do
  @moduledoc """
  The Polls context.
  """

  import Ecto.Query, warn: false
  alias Votebot.Repo
  import Logger
  import Ecto.Changeset

  alias Votebot.Polls.Poll
  import Slug, only: [slugify: 1]

  def fetch(query) do
    case Repo.all(query) do
      [] -> {:error, :not_found}
      [obj] -> {:ok, obj}
      _ -> raise "Expected one or no items, got many items #{inspect(query)}"
    end
  end

  @doc """
  Gets a single poll.

  Returns `nil` if the Poll does not exist.

  ## Examples

      iex> get_poll(123)
      %Poll{}

      iex> get_poll(456)
      ** nil

  """
  def get_poll(id), do: Repo.get(Poll, id)

  @doc """
  Gets a single poll.

  Raises `Ecto.NoResultsError` if the Poll does not exist.

  ## Examples

      iex> get_poll!(123)
      %Poll{}

      iex> get_poll!(456)
      ** (Ecto.NoResultsError)

  """
  def get_poll!(id), do: Repo.get!(Poll, id)

  def fetch_poll_by_selector(slug) when is_binary(slug) do
    answer =
      fetch(
        from p in Poll,
          # fetch(from p in "polls",
          where: p.slug == ^slug,
          select: p
      )

    Logger.debug("Fetched poll and got: #{inspect(answer)}")
    answer
  end

  def fetch_poll_by_selector(id) when is_integer(id) do
    answer =
      fetch(
        from p in Poll,
          where: p.id == ^id,
          select: p
      )

    Logger.debug("Fetched poll and got: #{inspect(answer)}")
    answer
  end

  @doc """
  Creates a poll.

  ## Examples

      iex> create_poll(%{field: value})
      {:ok, %Poll{}}

      iex> create_poll(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  # def create_poll(attrs, admins) do
  # def create_poll(attrs \\ %{}) do
  # def create_poll(%{"name" => name, "admins" => admins} = attrs \\ %{}) do
  # TODO: add name as mandatory attr
  def create_poll(attrs) do
    if not Map.has_key?(attrs, :slug) and Map.has_key?(attrs, :description) do
      slug =
        Map.get(attrs, :description)
        |> Slug.slugify()

      Map.put_new(attrs, :slug, slug)
    end

    %Poll{}
    # |> Poll.changeset(attrs)
    |> Poll.changeset(attrs) = poll
    |> Repo.insert()
    poll
    |> Ballot.from_poll()

    # FIXME: a extraire dansn une fonctions
    Polyjuice.Client.Room.send_message(
      client,
      "!room_id"
      %{"msgtype" => "pm.imago.ballot", "body" => "using full message content", "ballot" => ballot},
    )

  end

  @doc """
  Updates a poll.

  ## Examples

      iex> update_poll(poll, %{field: new_value})
      {:ok, %Poll{}}

      iex> update_poll(poll, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_poll(%Poll{} = poll, attrs) do
    poll
    |> Poll.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a poll.

  ## Examples

      iex> delete_poll(poll)
      {:ok, %Poll{}}

      iex> delete_poll(poll)
      {:error, %Ecto.Changeset{}}

  """
  def delete_poll(%Poll{} = poll) do
    Repo.delete(poll)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking poll changes.

  ## Examples

      iex> change_poll(poll)
      %Ecto.Changeset{data: %Poll{}}

  """
  def change_poll(%Poll{} = poll, attrs \\ %{}) do
    Poll.changeset(poll, attrs)
  end

  alias Votebot.Polls.PollOption

  @doc """
  Returns the list of poll_options.

  ## Examples

      iex> list_poll_options(poll_id)
      [%PollOption{}, ...]

  """

  def list_poll_options(poll_id) do
    # Repo.all(PollOption, poll: poll)
    Repo.all(from po in PollOption, where: po.poll_id == ^poll_id)
    # Repo.all(PollOption, poll: poll)
    |> Repo.preload(:poll)
  end

  def fetch_poll_option_by_selector(poll_id, number) when is_integer(number) do
    fetch(
      from po in PollOption, where: po.poll_id == ^poll_id and po.number == ^number, select: po
    )
  end

  def fetch_poll_option_by_selector(poll_id, name) when is_binary(name) do
    fetch(from po in PollOption, where: po.poll_id == ^poll_id and po.name == ^name, select: po)
  end

  def search_poll_option_by_name(poll_id, poll_option_name) do
    list_poll_options(poll_id)
    |> closest(poll_option_name, :name)
  end

  def closest(list_of_structs, string, key) do
    list_of_structs
    |> Enum.sort_by(fn struct -> String.jaro_distance(string, Map.get(struct, key)) end)
    |> List.first()
  end

  @doc """
  Creates a poll_option.

  ## Examples

      iex> create_poll_option(%{field: value})
      {:ok, %PollOption{}}

      iex> create_poll_option(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  # TODO: make it with poll_id instead of poll.id
  def create_poll_option(poll_id, attrs \\ %{}) when is_integer(poll_id) do
    new_poll_option_number =
      list_poll_options(poll_id)
      |> Enum.max_by(fn poll_option -> Map.get(poll_option, :number, nil) end, fn -> %{number: 0} end)
      |> Map.get(:number) |> Kernel.+(1)

    %PollOption{poll_id: poll_id, number: new_poll_option_number}
    |> PollOption.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a poll_option.

  ## Examples

      iex> update_poll_option(poll_option, %{field: new_value})
      {:ok, %PollOption{}}

      iex> update_poll_option(poll_option, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_poll_option(%PollOption{} = poll_option, attrs) do
    poll_option
    |> PollOption.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a poll_option.

  ## Examples

      iex> delete_poll_option(poll_option)
      {:ok, %PollOption{}}

      iex> delete_poll_option(poll_option)
      {:error, %Ecto.Changeset{}}

  """
  def delete_poll_option(%PollOption{} = poll_option) do
    Repo.delete(poll_option)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking poll_option changes.

  ## Examples

      iex> change_poll_option(poll_option)
      %Ecto.Changeset{data: %PollOption{}}

  """
  def change_poll_option(%PollOption{} = poll_option, attrs \\ %{}) do
    PollOption.changeset(poll_option, attrs)
  end
end
