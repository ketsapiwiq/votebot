defmodule Votebot.Polls.Poll do
  use Ecto.Schema
  import Ecto.Changeset

  schema "polls" do
    field :description, :string
    field :slug, :string
    field :room_id, :string
    field :creator, :string
    # field :end_date, :date
    # field :modality, :atom
    # field :continuous, :bool
    timestamps()
  end

  @doc false
  # def changeset(%Votebot.Polls.Poll{"name" => name} = poll, admins) do
  def changeset(poll, attrs) do
    # def changeset(poll, attrs, admins) do
    # def changeset(poll, %{"name" => name, "admins" => admins} = attrs) do
    poll
    |> cast(attrs, [:slug, :description, :creator, :room_id])
    |> validate_required(:room_id)

    # |> validate_required([:description, :room_id, :slug])
    # |> unique_constraint([:slug, :description])

    # |> put_assoc(:admins, admins)
  end
end
