defmodule Votebot.Polls.PollOption do
  use Ecto.Schema
  import Ecto.Changeset
  alias Votebot.Polls.Poll

  schema "poll_options" do
    field :name, :string
    field :number, :integer
    belongs_to :poll, Poll

    timestamps()
  end

  @doc false
  def changeset(poll_option, attrs) do
    poll_option
    |> cast(attrs, [:name, :poll_id, :number])
    |> validate_required([:name, :poll_id, :number])
    |> unique_constraint([:name, :poll_id])
    |> unique_constraint([:number, :poll_id])
  end
end
