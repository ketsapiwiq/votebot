defmodule Votebot.Repo do
  use Ecto.Repo,
    otp_app: :votebot,
    adapter: Ecto.Adapters.Postgres
end
