defmodule Votebot.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    base_url = Application.get_env(:votebot, :matrix) |> Keyword.fetch!(:base_url)
    access_token = Application.get_env(:votebot, :matrix) |> Keyword.fetch!(:access_token)
    user_id = Application.get_env(:votebot, :matrix) |> Keyword.fetch!(:user_id)

    children = [
      # Start the Ecto repository
      Votebot.Repo,
      # Start a worker by calling: Votebot.Worker.start_link(arg)
      {Votebot, [base_url, access_token, user_id]}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Votebot.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
