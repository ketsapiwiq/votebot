defmodule Votebot.Event do
  @moduledoc """
  Ecto schema for a Matrix event.
  """
  use Ecto.Schema
  import Ecto.Changeset

  # @type t :: %__MODULE__{
  #         age: integer(),
  #         content: map(),
  #         event_id: String.t(),
  #         redacts: String.t(),
  #         origin_server_ts: integer(),
  #         room_id: String.t(),
  #         sender: String.t(),
  #         state_key: String.t(),
  #         type: String.t(),
  #         unsigned: map(),
  #         user_id: String.t()
  #       }

  # defstruct age: nil,
  #           content: %{},
  #           event_id: nil,
  #           redacts: nil,
  #           origin_server_ts: nil,
  #           room_id: nil,
  #           sender: nil,
  #           state_key: "",
  #           type: nil,
  #           unsigned: %{},
  #           user_id: nil

  embedded_schema do
    field(:age, :integer)
    field(:content, :map)
    field(:event_id, :string)
    field(:redacts, :string)
    field(:origin_server_ts, :integer)
    field(:room_id, :string)
    field(:sender, :string)
    field(:state_key, :string)
    field(:type, :string)
    field(:unsigned, :map)
    field(:user_id, :string)
  end

  def new(params) do
    command = changeset(params)

    case command.valid? do
      true -> {:ok, apply_changes(command)}
      false -> {:error, command.errors}
    end
  end

  defp changeset(params) do
    %__MODULE__{}
    |> cast(params, [
      :age,
      :content,
      :event_id,
      :redacts,
      :origin_server_ts,
      :room_id,
      :sender,
      :state_key,
      :type,
      :unsigned,
      :user_id
    ])
    |> validate_required([
      :content,
      :event_id,
      :origin_server_ts,
      :room_id,
      :sender,
      :type,
      :unsigned
    ])
  end
end
