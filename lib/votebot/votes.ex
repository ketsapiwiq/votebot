defmodule Votebot.Votes do
  @moduledoc """
  The Votes context.
  """

  import Ecto.Query, warn: false
  alias Votebot.Repo

  alias Votebot.Votes
  alias Votebot.Votes.Vote
  # alias Votebot.Polls
  alias Votebot.Polls.Poll
  alias Votebot.Polls.PollOption

  @topic inspect(__MODULE__)

  @doc """
  Returns the list of votes.

  ## Examples

      iex> list_votes()
      [%Vote{}, ...]

  """
  def list_votes do
    Repo.all(Vote)
    |> Repo.preload(:poll)
    |> Repo.preload(:poll_option)
  end

  @doc """
  Gets a single vote.

  Raises `Ecto.NoResultsError` if the Vote does not exist.

  ## Examples

      iex> get_vote!(123)
      %Vote{}

      iex> get_vote!(456)
      ** (Ecto.NoResultsError)

  """
  def get_vote!(id), do: Repo.get!(Vote, id)

  @doc """
  Creates a vote.

  ## Examples

      iex> create_vote(%{field: value})
      {:ok, %Vote{}}

      iex> create_vote(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_vote(attrs \\ %{}) do
    %Vote{}
    |> Vote.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a vote.

  ## Examples

      iex> update_vote(vote, %{field: new_value})
      {:ok, %Vote{}}

      iex> update_vote(vote, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_vote(%Vote{} = vote, attrs) do
    vote
    |> Vote.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a vote.

  ## Examples

      iex> delete_vote(vote)
      {:ok, %Vote{}}

      iex> delete_vote(vote)
      {:error, %Ecto.Changeset{}}

  """
  def delete_vote(%Vote{} = vote) do
    Repo.delete(vote)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking vote changes.

  ## Examples

      iex> change_vote(vote)
      %Ecto.Changeset{data: %Vote{}}

  """
  def change_vote(%Vote{} = vote, attrs \\ %{}) do
    Vote.changeset(vote, attrs)
  end

  # def process_vote_command(vote_command, room_id, sender) do
  #   case(Votes.parse_vote_command(vote_command)) do
  #     {:ok, poll, poll_option} ->
  #       if poll.room == room_id do
  #         Votes.create_vote(poll_option_id: poll_option.id, poll_id: poll.id, user_id: sender)
  #       end
  #   end
  # end

  # def parse_vote_command([poll_name, poll_option_value]) do
  #   # raise "Not implemented"
  #   case search_poll_option_by_name(poll_name) do
  #     {:ok, poll} ->
  #       case search_poll_option_by_name(poll_option_value) do
  #         {:ok, poll_option} -> {:ok, poll, poll_option}
  #       end
  #   end
  # end
end
