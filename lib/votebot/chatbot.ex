defmodule Votebot.Chatbot do
  # Votebot.Events.handle_message("!OcyJSWAkRoVHqPQyFB:votebot.local", "@test2:votebot.local", %{"body" => "Unknown command: !votebot hola", "msgtype" => "m.notice"})
  require Logger
  alias Votebot.Polls
  alias Votebot.Polls.PollOption
  alias Votebot.Polls.Poll
  alias Votebot.Votes

  def send_log(log, room_id, level \\ :info) do
    Polyjuice.Client.get_client(Votebot.Client)
    |> Polyjuice.Client.Room.send_message(
      room_id,
      %{"body" => log, "msgtype" => "m.notice"}
    )
    Logger.log(level, log)
  end

  def parse_poll_selector(command) do
    with string <-
           String.split(command)
           |> List.first() do
      case Integer.parse(string) do
        {id, _} -> {:ok, id}
        :error -> {:ok, string}
      end
    else
      _ -> {:error, "Could not parse any poll selector from #{inspect(command)}"}
    end
  end
# TODO: to refacto with previous function
  defp parse_poll_option_selector(command) do
    selector =
      String.split(command)
      |> Enum.at(1)

    case Integer.parse(selector) do
      {id, _} -> {:ok, id}
      :error -> {:ok, selector}
    end
  end


  def parse_poll_desc(command) do
    split_command = String.split(command, "\n")

    cond do
      split_command |> Enum.count() == 1 ->
        {:ok, String.split(command) |> Enum.drop(1) |> Enum.join(" ")}

      split_command |> Enum.count() >= 2 ->
        {:ok,
         split_command
         |> Enum.drop(1)
         |> Enum.join("\n")
         |> String.trim()}

      true ->
        {:error, "Failed parsing desc for command #{command}"}
    end
  end

  # # TODO: check if it wants to create a poll, else room_id needs to be a room with a poll related to it
  # def handle_message(room_id, sender, body)  do
  #   # TODO: check if user_id is of form @user:domain
  #   Logger.debug("ignored message from bot")
  #   {:ok, :ignored_from_bot}
  # end

  def handle_message(room_id, sender, "!poll new") do
    # TODO: test if sender is admin of room?

    {:ok, poll} =
      Polls.create_poll(%{
        "room_id" => room_id,
        "creator" => sender
      })

    log = ~s"Created poll ID #{poll.id}\n
Please give it a description now by typing:\n
!poll desc #{poll.id}\n
This is the description of poll ID #{poll.id}."

    # TODO: you can also give it a title with !poll title #{poll.id}
    # TODO: or by quoting this message

    send_log(log, room_id)
    {:ok, poll}
  end

  def handle_message(room_id, _sender, "!poll desc" <> command) do
    Logger.debug("parsing poll desc")
    # TODO: test if sender is admin / poll creator?
    with {:ok, poll_selector} <- parse_poll_selector(command),
         {:ok, %Polls.Poll{} = poll} <- Polls.fetch_poll_by_selector(poll_selector) do
      Logger.debug("got poll: " <> inspect(poll))
      #  {:ok, poll}

      # with poll_desc when is_binary(poll_desc) <-  do
      with {:ok, poll_desc} when is_binary(poll_desc) <- parse_poll_desc(command) do
        Polls.update_poll(poll, %{
          "description" => poll_desc
        })

        # TODO: sanitize desc?
        log = ~s'Updated poll ID #{poll.id} with description:\n
#{poll_desc}'

        send_log(log, room_id)
        {:ok, log}
      else
        {:error, _} = err -> err
        err -> err
      end
    else
      {:error, err} -> {:error, "Could not parse poll ID in #{command}, got error: #{err}"}
      # :ok -> {:error, "LOLILOL"}
      error -> {:error, "Poll not found: #{inspect(error)}"}
    end
  end

  def handle_message(room_id, _sender, "!poll_option new" <> command) do
    with {:ok, poll_selector} <- parse_poll_selector(command),
         {:ok, %Polls.Poll{} = poll} <- Polls.fetch_poll_by_selector(poll_selector),
         # poll_option_name when is_binary(poll_option_name) <- String.split(command) |> Enum.drop(3)
         poll_option_name when is_binary(poll_option_name) <-
           String.split(command) |> Enum.drop(1) |> Enum.join(" ") do
      # Logger.debug("got polloption name: #{poll_option_name}")
      case Polls.create_poll_option(poll.id, %{
               name: poll_option_name
             }) do
        {:ok, %PollOption{number: poll_option_number}} ->
          log =
            ~s'Added option n°`#{poll_option_number}`: `#{poll_option_name}` to poll ID `#{poll.id}`'
            |> send_log(room_id)

          Logger.info(log)
          {:ok, log}
        {:error, %Ecto.Changeset{errors: [name: {"can't be blank", [validation: :required]}]}} -> log = "Poll option needs a name"
        send_log(log, room_id, :error)
        {:error, :option_name_missing}
        {:error, %Ecto.Changeset{errors: [{"has already been taken", [constraint: :unique, constraint_name: "poll_options_name_poll_id_index"]}]}} ->
          "Poll option already exists"
           |> send_log(room_id)
            {:error, :already_exists}

            {:error, %Ecto.Changeset{errors: err}} ->
              {:error,
               "Ecto errors #{inspect(err)} when adding poll option: #{inspect(poll_option_name)}"}
            other ->
          {:error,
           "unknown error #{inspect(other)} when adding poll option: #{inspect(poll_option_name)}"}
      end
    else
      {:error, :not_found} -> log = "Could not find poll ID"
      send_log(log, room_id)
        {:error, log}

      {:error, err} ->
        {:error, "Could not parse poll id when adding poll option: #{inspect(err)}"}

      other ->
        {:error,
         "unknown error #{inspect(other)} when adding poll option with: #{inspect(command)}"}

        # TODO: Add incremented id poll_option specific to one poll and output it here
    end
  end

  def handle_message(room_id, sender, "!vote " <> command) do
    with {:ok, poll_selector} <- parse_poll_selector(command),
         {:ok, %Poll{} = poll} <- Polls.fetch_poll_by_selector(poll_selector),
         {:ok, poll_option_selector} <- parse_poll_option_selector(command),
         {:ok, %PollOption{} = poll_option} <-
           Polls.fetch_poll_option_by_selector(poll.id, poll_option_selector) do
      # TODO: check if sender can vote, or already voted (can modify vote?)
      {:ok, vote} =
        Votes.create_vote(%{
          "user_id" => sender,
          "poll_id" => poll.id,
          "poll_option_id" => poll_option.id
        })

      log =
        "Created vote #{inspect(vote)} for poll_option #{inspect(poll_option)} in poll #{inspect(poll)}"

      send_log(log, room_id)
      {:ok, vote}
    else
      # TODO: differentiate if poll option not in poll?
      {:error, err} ->
        {:error, "Could not find poll or poll option from #{command}: #{inspect(err)}"}

      err ->
        {:error, "Could not parse vote command from #{command}: #{inspect(err)}"}
        # _ -> {:error, "Poll not found, got #{inspect(poll)}"}
        log =
          "Vote not parsed, String.split(command) gave: #{inspect(command)}"
          |> send_log(room_id, :warn)

        {:error, log}
    end
  end

  def handle_message(_room_id, _sender, body) do
    log = "Ignored body because not prefixed: #{body}"
    Logger.debug(log)

    {:ok, :no_prefix}
  end
end
