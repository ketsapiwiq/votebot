defmodule Votebot do
  @moduledoc """
  Votebot keeps the contexts that define your domain
  and business logic.

  Contexts are also responsible for managing your data, regardless
  if it comes from the database, an external API or others.
  """

  import Logger
  use GenServer
  alias Votebot.Votes
  alias Votebot.Polls
  alias Votebot.Events
  alias Polyjuice.Client.Filter

  @spec start_link(any) :: :ignore | {:error, any} | {:ok, pid}
  def start_link([base_url, access_token, user_id]) do
    GenServer.start_link(__MODULE__, [base_url, access_token, user_id])
  end

  @impl true
  def init([base_url, access_token, user_id]) do
    {:ok, pid, client} =
      Polyjuice.Client.start_link_and_get_client(
        base_url,
        access_token: access_token,
        user_id: user_id,
        storage: Polyjuice.Client.Storage.Dets.open("data/polyjuice_client_kvstore.dets"),
        # storage: Polyjuice.Client.Storage.Ets.open(),
        # - `handler`: (required for sync) an event handler (see `Polyjuice.Client.Handler`)
        # ...>   sync_filter: Filter.include_presence_types([])
        # ...>     |> Filter.include_ephemeral_types([])
        # ...>     |> Filter.include_state_types(["m.room.member"])
        # ...>     |> Filter.include_timeline_types(["m.room.member"])
        # sync_filter: Filter.include_timeline_types(["m.room.message"]),
        # |> Filter.include_presence_types([])
        # |> Filter.include_ephemeral_types([]),
        # |> Filter.include_state_types([]),
        # The alternative to this PID / message approach is to implement the Polyjuice.Client.Handler protocol with a handle/2 function
        # If a function is used as a handler, it must have arity 2, and will be given
        # `type` and `args` as its arguments.
        handler: self(),
        # sync_full_state ?
        # - `full_state:` (boolean) whether to return the full room state instead of
        #   just the state that has changed since the last sync

        name: Votebot.Client
      )

    Logger.debug("Polyjuice pid: #{inspect(pid)}")
    Logger.debug("Votebot pid: #{inspect(self())}")

    {:ok, client, {:continue, nil}}
    # loop(client)
    # {:ok, :running}
  end

  @impl true
  def handle_continue(_continue, client) do
    loop(client)
  end

  # TODO: sync previous messages or store sync state
  #   - `filter:` (string or map) a filter to apply to the sync.  May be either the
  #   ID of a previously uploaded filter, or a new filter.
  # - `since:` (string) where to start the sync from.  Should be a token obtained
  #   from the `next_batch` of a previous sync.
  # - `full_state:` (boolean) whether to return the full room state instead of
  #   just the state that has changed since the last sync

  # The alternative to this PID / message approach is to implement the Polyjuice.Client.Handler protocol with a handle/2 function
  # If a function is used as a handler, it must have arity 2, and will be given
  # `type` and `args` as its arguments.
  def loop(client) do
    receive do
      {:polyjuice_client, :limited, {room_id, prev_batch, _state_events}} ->
        # if Application.get_env(:votebot, :fetch_earlier_events) do
        Logger.info("Processing earlier event than "<> prev_batch)
          process_earlier_events(client, room_id, prev_batch)
        # end

      # TODO: parse (just once) state events?
      # process_state_events()

      {:polyjuice_client, :invite, {room_id, _inviter, _invite_state}} ->
        Polyjuice.Client.Room.join(client, room_id)

      {:polyjuice_client, event_type, event} ->
        Logger.debug("[#{inspect(event_type)}, #{inspect(event)}]")
        # try do
        Events.handle_event(event_type, event)

      # rescue
      #   e ->
      #     Logger.error(
      #       "Error handling event: " <>
      #         Exception.format(:error, e, __STACKTRACE__) <> "\nEvent: #{inspect(event)}"
      #     )
      # end

      {:polyjuice_client, event_type} ->
        Logger.debug("#{inspect(event_type)}")

        Events.handle_event(event_type)

      other ->
        Logger.warn("Not handling: #{inspect(other)}")
    end

    loop(client)
  end

  # [debug] Unhandled event: :message
  # Details: {"!RREjCyllLhpWDdCtul:votebot.local", %{"content" => %{"m.relates_to" => %{"event_id" => "$g_oJ4Q2ma_p0fH65BXenXcRf2vdwFDWkBavmuaCC914", "key" => "😊", "rel_type" => "m.annotation"}}, "event_id" => "$8TwjFmJuk3-uo58aOM1u9ecU4E7rC_v8Ph840LoDxpI", "origin_server_ts" => 1642255641687, "sender" => "@test2:votebot.local", "type" => "m.reaction", "unsigned" => %{"age" => 130, "transaction_id" => "m1642255641463.5"}}}

  def process_earlier_events(client, room_id, prev_batch, limit \\ 0) do
    Logger.debug("Handling prev_batch: #{inspect(prev_batch)}")
    # if events && events != [] do
    # Create a stream of events.  `stream_messages` gives a stream of message
    # batches as given by `GET /rooms/{roomid}/messages`.

    # TODO: why do we get state messages here as well? I feel we're parsing them twice

    event_stream =
      if prev_batch do
        Polyjuice.Client.Room.stream_messages(
          client,
          room_id,
          prev_batch,
          :backward
        )
        # Fetch the "chunk" property from each batch, which is a list of events
        # in that batch (from latest to earliest, since the direction is
        # `:backward`).
        |> Stream.map(&Map.get(&1, "chunk", []))
        # Concatenate the events from the stream of batches to get a single
        # stream of events.
        |> Stream.concat()

        # Prepend the events that we got from the sync.
        # |> (&Stream.concat(events, &1)).()
        # else
        #   events
      end

    # Traverse the stream, and take messages until we get to one earlier than
    # our limit.
    events =
      event_stream
      |> Stream.take_while(&(Map.get(&1, "origin_server_ts", 0) >= limit))
      # Reverse the stream so that we go from earliest to latest
      |> Enum.reverse()

    # Format the event
    # |> Enum.each(&IO.puts("#{Map.get(&1, "sender")}> #{Map.get(&1, "content") |> Map.get("body")}"))

    events
    # TODO: refacto, this is dumb (or is it?)
    |> Enum.each(fn event -> Events.handle_event(event_type(event), {room_id, event}) end)

    # Stream.each(fn %{"type" => event_type}= event -> Events.handle_event(event_type, event) end)
  end

  def event_type(event) do
    case event do
      %{"state_key" => _} -> :state
      _ -> :message
    end
  end
end
