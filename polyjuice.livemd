# Votebot playground

## Testing Polyjuice

<https://gitlab.com/polyjuice/polyjuice_client/-/blob/master/tutorial_echo.md>

```elixir
r = "!pLielceyyStqIIDKKe:votebot.local"
u = "votebot_votebot"
```

```elixir
{:ok, pid} =
  Polyjuice.Client.start_link(
    "http://matrix.votebot.local",
    access_token:
      "MDAxYmxvY2F0aW9uIHZvdGVib3QubG9jYWwKMDAxM2lkZW50aWZpZXIga2V5CjAwMTBjaWQgZ2VuID0gMQowMDI3Y2lkIHVzZXJfaWQgPSBAdGVzdDI6dm90ZWJvdC5sb2NhbAowMDE2Y2lkIHR5cGUgPSBhY2Nlc3MKMDAyMWNpZCBub25jZSA9IFVeZEE1aDRkZEJqeS04Li4KMDAyZnNpZ25hdHVyZSB4eXr_zzgbaqKzKqETc8D4Hf467etpqpD7NjGTXwCLjQo",
    user_id: "@votebot_votebot:votebot.local",
    storage: Polyjuice.Client.Storage.Ets.open(),
    handler: self()
  )

client = Polyjuice.Client.get_client(pid)
```

```elixir
import Logger

defmodule Echobot do
  def loop(client) do
    receive do
      {:polyjuice_client, :message,
       {room_id, %{"content" => %{"msgtype" => "m.notice"} = content}}} ->
        Polyjuice.Client.Room.send_message(
          client,
          room_id,
          %{content | "msgtype" => "m.notice"}
        )

      {:polyjuice_client, :message, {room_id, %{"content" => %{"msgtype" => "m.text"} = content}}} ->
        Polyjuice.Client.Room.send_message(
          client,
          room_id,
          %{content | "msgtype" => "m.text"}
        )

      {:polyjuice_client, :invite, {room_id, _inviter, _invite_state}} ->
        Polyjuice.Client.Room.join(client, room_id)

      {:polyjuice_client, event, _} ->
        Logger.debug("Unhandled event: " <> inspect(event))

      _ ->
        nil
    end

    loop(client)
  end
end
```

```elixir
Echobot.loop(client)
```
