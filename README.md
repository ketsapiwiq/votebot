# Votebot

This program acts as a POC of democracy features over Matrix.

## Get started

To start the Matrix bot:

- Install dependencies with `mix deps.get`
- Launch the database, e.g. with `systemctl start postgresql.service`
- Change `config/config.exs`
- Create and migrate your database with `mix ecto.setup`
- Then run:

```elixir
mix deps.get
mix ecto.setup
mix run
```

## Concepts
- See docs here: 

### Synapse container

```yaml
docker-compose run matrix generate
```

## Stories

### Tours de parole dans une AG

- Un ordre du jour et des temps prévus
- On s'inscrit à chaque thème
- Une période de 2min par défaut
- Une réaction de 30s avec un système de queue
- des types de vote : préférentiel (trié, Ranked Choice Voting), note sur 10 de chaque choix, vote à 1 tour ou 2 tours avec threshold
- pouvoir modérer le chat en live et poser des questions par écrit

![](topos.jpg)

### Amendements d'un document
Voir story de Wobbly.app

### Kick/ban de quelqu'un

### Désignation d'un-e mandaté-e

### Vote d'un budget/d'une dépense / d'un don / d'un échange de biens
Avec Valueflows